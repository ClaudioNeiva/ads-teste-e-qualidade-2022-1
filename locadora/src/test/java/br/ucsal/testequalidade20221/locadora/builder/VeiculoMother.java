package br.ucsal.testequalidade20221.locadora.builder;

import java.time.LocalDate;

import br.ucsal.testequalidade20221.locadora.dominio.Modelo;
import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20221.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoMother {

	private static Modelo modelo1 = new Modelo("Gol");

	public static Veiculo umVeiculoDisponivelAntigo() {
		Veiculo veiculo1 = new Veiculo();
		veiculo1.setPlaca("ABC-1234");
		veiculo1.setAnoFabricacao(LocalDate.now().getYear() - 20);
		veiculo1.setValorDiaria(115.0);
		veiculo1.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo1.setModelo(modelo1 );
		return veiculo1;
	}
	
	public static Veiculo umVeiculoDisponivelNovoBarato() {
		Veiculo veiculo1 = new Veiculo();
		veiculo1.setPlaca("BCD-3456");
		veiculo1.setAnoFabricacao(LocalDate.now().getYear() -  1);
		veiculo1.setValorDiaria(115.0);
		veiculo1.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo1.setModelo(modelo1 );
		return veiculo1;
	}
	
	public static Veiculo umVeiculoDisponivelNovoCaro() {
		Veiculo veiculo1 = new Veiculo();
		veiculo1.setPlaca("EFG-5678");
		veiculo1.setAnoFabricacao(LocalDate.now().getYear() -  1);
		veiculo1.setValorDiaria(315.0);
		veiculo1.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		veiculo1.setModelo(modelo1 );
		return veiculo1;
	}
	
}
