package br.ucsal.testequalidade20221.locadora.business;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import br.ucsal.testequalidade20221.locadora.builder.VeiculoBuilder;
import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20221.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.testequalidade20221.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
class LocacaoBOTest {

	private VeiculoDAO veiculoDAOMock;
	private LocacaoBO locacaoBO;

	@BeforeEach
	void setup() {
		veiculoDAOMock = Mockito.mock(VeiculoDAO.class);
		locacaoBO = new LocacaoBO(veiculoDAOMock);
	}

	/**
	 * Testar o cálculo do valor total de um contrato de locação com 5 dias de
	 * duração, 4 veículos locados, 3 fabricados há 2 anos e 1 fabricado há 15 anos.
	 * 
	 * @throws VeiculoNaoEncontradoException
	 */
	@Test
	void testarCalculoValorTotalLocacao4Veiculo5Dias() throws VeiculoNaoEncontradoException {
		int anoHa2Anos = LocalDate.now().getYear() - 2;
		int anoHa15Anos = LocalDate.now().getYear() - 15;
		VeiculoBuilder veiculoBuilder = VeiculoBuilder.umVeiculoNovo().fabricadoEm(anoHa2Anos).comValorDiaria(100d);
		Veiculo veiculo1 = veiculoBuilder.mas().comPlaca("ABC-1234").build();
		Veiculo veiculo2 = veiculoBuilder.mas().comPlaca("ABC-1235").build();
		Veiculo veiculo3 = veiculoBuilder.mas().comPlaca("ABC-1238").build();
		Veiculo veiculo4 = veiculoBuilder.mas().comPlaca("AAA-1122").fabricadoEm(anoHa15Anos).build();

		List<Veiculo> veiculosTest = Arrays.asList(veiculo1, veiculo2, veiculo3, veiculo4);
		List<String> placasTest = Arrays.asList(veiculo1.getPlaca(), veiculo2.getPlaca(), veiculo3.getPlaca(),
				veiculo4.getPlaca());
		
		Mockito.when(veiculoDAOMock.obterPorPlacas(placasTest)).thenReturn(veiculosTest);
		
		Integer quantidadeDiasLocacao = 5;
		double valorTotalEsperado = 1950;

		double valorTotalAtual = locacaoBO.calcularValorTotalLocacao(placasTest, quantidadeDiasLocacao, LocalDate.now());

		Assertions.assertEquals(valorTotalEsperado, valorTotalAtual);
	}

}
