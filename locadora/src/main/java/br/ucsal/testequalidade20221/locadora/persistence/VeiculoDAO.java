package br.ucsal.testequalidade20221.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20221.locadora.exception.VeiculoNaoEncontradoException;

public class VeiculoDAO {

	private List<Veiculo> veiculos = new ArrayList<>();

	// FIXME Erro introduzido na classe para avaliar o impacto do teste unitário x
	// teste integrado.
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		List<Veiculo> veiculosSelecionados = new ArrayList<>();
		for (String placa : placas) {
			veiculosSelecionados.add(obterPorPlaca(placa));
		}
		return veiculosSelecionados;
//		return veiculos.subList(0, 2);
	}

	public Veiculo obterPorPlaca(String placa) throws VeiculoNaoEncontradoException {
		for (Veiculo veiculo : veiculos) {
			if (veiculo.getPlaca().equalsIgnoreCase(placa)) {
				return veiculo;
			}
		}
		throw new VeiculoNaoEncontradoException();
	}

	public void insert(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

}
