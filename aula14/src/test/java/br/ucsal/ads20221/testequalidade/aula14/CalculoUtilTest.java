package br.ucsal.ads20221.testequalidade.aula14;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoUtilTest {

	@Test
	void testarFatorial0() {
		int n = 0;
		Long fatorialEsperado = 1L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
	@Test
	void testarFatorial5() {
		int n = 5;
		Long fatorialEsperado = 120L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
	@Test
	void testarFatorial6() {
		int n = 6;
		Long fatorialEsperado = 720L;
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
//	@Test
//	void testarNumeroPerfeito() {
//		int n = ???;
//		Boolean isPerfeitoEsperado = ????;
//		Boolean isPerfeitoAtual = CalculoUtil.verificarNumeroPerfeito(n);
//		Assertions.assertEquals(isPerfeitoEsperado, isPerfeitoAtual);
//	}

}
