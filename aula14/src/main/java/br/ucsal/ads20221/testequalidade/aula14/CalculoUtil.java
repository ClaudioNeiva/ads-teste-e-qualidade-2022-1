package br.ucsal.ads20221.testequalidade.aula14;

public class CalculoUtil {

	private CalculoUtil() {
	}

	public static Long calcularFatorial(int n) {
		Long fat = 1L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

//	public static Boolean verificarNumeroPerfeito(int n) {
//		return null;
//	}

}
