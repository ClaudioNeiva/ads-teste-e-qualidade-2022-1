package br.ucsal.ads20221.testequalidade.escolajbehave;

import java.util.Arrays;
import java.util.List;

import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.Before;
import org.junit.BeforeClass;

public class LancamentoNotaTest extends AbstractStoryTest {

	@BeforeClass
	public static void setupClass() {
		System.out.println("setupClass");
	}

	@Before
	public void setup() {
		System.out.println("setup");
	}

	@Override
	protected List<String> storyPaths() {
		return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()),
				Arrays.asList("**/lancamento-nota.story"), Arrays.asList(""));
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		return new InstanceStepsFactory(configuration(), new LancamentoNotaSteps());
	}

}
