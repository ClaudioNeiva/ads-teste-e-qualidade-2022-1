package br.ucsal.ads20221.testequalidade.escolajbehave;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExemploAnotacao {

	@BeforeClass
	public static void setupClass() {
		// no início do teste
		System.out.println("BeforeClass");
	}

	@Before
	public void setup() {
		// antes de cada teste
		System.out.println("Before");
		// meu método de limpeza não funciona???
	}

	@Test
	public void test1() {
		System.out.println("teste1");
	}

	@Test
	public void test2() {
		System.out.println("teste2");
	}

}
