package br.ucsal.ads20221.testequalidade.escolajbehave;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LancamentoNotaSteps {

	private Aluno aluno;

	/*
	 * O "E" se comporta de acordo com o termo da linha anterior ao mesmo. Ou seja,
	 * um "E" abaixo de um "Dado que", funciona como um "Dado que". Um "E" abaixo de
	 * um "Quando" funciona como um "Quando"; Um "E" abaixo de um "Então" funciona
	 * como um "Então".
	 */

	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		aluno = new Aluno();
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		aluno.informarNota(nota);
	}

	@Then("a situação do aluno é $situacao")
	public void verificarSituacaoAluno(String situacaoEsperada) {
		String situacaoAtual = aluno.obterSituacao();
		Assert.assertEquals(situacaoEsperada, situacaoAtual);
	}

}
