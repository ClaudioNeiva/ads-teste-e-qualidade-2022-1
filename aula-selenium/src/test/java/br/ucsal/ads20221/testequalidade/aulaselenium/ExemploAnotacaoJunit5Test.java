package br.ucsal.ads20221.testequalidade.aulaselenium;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExemploAnotacaoJunit5Test {

	@BeforeAll
	void setupClass() {
		// no início do teste
		System.out.println("BeforeClass");
	}

	@BeforeEach
	void setup() {
		// antes de cada teste
		System.out.println("Before");
		// meu método de limpeza não funciona???
	}

	@Test
	void test1() {
		System.out.println("teste1");
	}

	@Test
	void test2() {
		System.out.println("teste2");
	}
	
	
}
