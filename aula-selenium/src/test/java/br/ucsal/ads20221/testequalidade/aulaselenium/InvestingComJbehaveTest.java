package br.ucsal.ads20221.testequalidade.aulaselenium;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InvestingComJbehaveTest {

	private static WebDriver driver;
	private static WebDriverWait wait;

	@BeforeAll
	public static void setup() {
		// carregarDriverChrome();
		carregarDriverFirefox();
		wait = new WebDriverWait(driver, 5);
	}

	private static void carregarDriverFirefox() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver_0_31.exe");
		// System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
		driver = new FirefoxDriver();
	}

	private static void carregarDriverChrome() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver_101.exe");
		driver = new ChromeDriver();
	}

	@AfterAll
	public static void teardown() {
		driver.close();
	}

	@ParameterizedTest
	public void testarPesquisa() throws InterruptedException {
		// Abrir página do Investing.com
		driver.get("http://br.investing.com");

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);
		pesquisarNoSiteInput.getAttribute("innerHTML");

		// Obter o conteúdo da página
		// Thread.sleep(5000);
		wait.until(ExpectedConditions.numberOfElementsToBe(By.className("js-inner-all-results-quotes-wrapper"), 1));
		String conteudo = driver.getPageSource();

		// Verificar se retorno inclui "Cogna Educação"
		Assertions.assertTrue(conteudo.contains("Cogna Educacao"));
	}

}
