package br.ucsal.ads20221.testequalidade.aulaselenium;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InvestingComTest {

	private static final long TIMEOUT_WAIT = 20;

	@ParameterizedTest
	@MethodSource("fornecerDrivers")
	public void testarPesquisa(WebDriver driver) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT_WAIT);
		
		// Abrir página do Investing.com
		driver.get("http://br.investing.com");

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);
		pesquisarNoSiteInput.getAttribute("innerHTML");

		// Obter o conteúdo da página
		// Thread.sleep(5000);
		wait.until(ExpectedConditions.numberOfElementsToBe(By.className("js-inner-all-results-quotes-wrapper"), 1));
		String conteudo = driver.getPageSource();

		// Verificar se retorno inclui "Cogna Educação"
		Assertions.assertTrue(conteudo.contains("Cogna Educacao"));
		
		driver.close();
	}

	private static Stream<Arguments> fornecerDrivers() {
	    return Stream.of(
	  	      Arguments.of(obterDriverChrome()),
		      Arguments.of(obterDriverFirefox())
	    );
	}

	
	private static FirefoxDriver obterDriverFirefox() {
		System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver_0_31.exe");
		// System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
		return new FirefoxDriver();
	}

	private static ChromeDriver obterDriverChrome() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver_102.exe");
		return new ChromeDriver();
	}

}
