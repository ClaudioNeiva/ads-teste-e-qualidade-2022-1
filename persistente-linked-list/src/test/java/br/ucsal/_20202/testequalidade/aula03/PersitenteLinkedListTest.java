package br.ucsal._20202.testequalidade.aula03;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

class PersitenteLinkedListTest {

	private static final String NOME_TABELA = "lista1";
	
	private static Connection conexao;
	private static Long idLista;

	private PersistentList<String> nomes;

	@BeforeAll
	static void setupClass() throws SQLException {
		Assumptions.assumeTrue(DbUtil.isConnectionValid());
		conexao = DbUtil.getConnection();
		idLista = 1L;
	}

	@BeforeEach
	void setup() throws SQLException {
		nomes = new PersitenteLinkedList<>();
		nomes.delete(idLista, conexao, NOME_TABELA);
	}

	void teardown() throws SQLException {
		nomes.delete(idLista, conexao, NOME_TABELA);
	}
	
	@Test
	void testarPersistenciaLista3Elementos()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {

		// Dados de entrada
		String nome0 = "antonio";
		String nome1 = "claudio";
		String nome2 = "neiva";
		nomes.add(nome0);
		nomes.add(nome1);
		nomes.add(nome2);
		int tamanhoEsperado = 3;

		// Executar o método que está sendo testado
		nomes.persist(idLista, conexao, NOME_TABELA);

		// Obter o dados atuais
		PersistentList<String> nomesAtuais = new PersitenteLinkedList<>();
		nomesAtuais.load(idLista, conexao, NOME_TABELA);

		// Comparar a lista esperado com a lista atual.
		Assertions.assertAll(() -> Assertions.assertEquals(nome0, nomesAtuais.get(0)),
				() -> Assertions.assertEquals(nome1, nomesAtuais.get(1)),
				() -> Assertions.assertEquals(nome2, nomesAtuais.get(2)),
				() -> Assertions.assertEquals(tamanhoEsperado, nomesAtuais.size()));

	}

	@Test
	void testar2() {
	}

	@Test
	void testar3() {
	}
}
