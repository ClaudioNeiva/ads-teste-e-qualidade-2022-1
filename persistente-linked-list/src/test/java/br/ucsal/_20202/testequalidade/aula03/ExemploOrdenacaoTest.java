package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ExemploOrdenacaoTest {

	@Test
	@DisplayName("caju")
	@Order(1)
	void testarA() {
		System.out.println("A");		
	}

	@Test
	@DisplayName("jaca")
	@Order(4)
	void testarB() {
		System.out.println("B");		
	}
	
	@Test
	@DisplayName("manga")
	@Order(2)
	void testarC() {
		System.out.println("C");		
	}
	
	@Test
	@DisplayName("goiaba")
	@Order(3)
	void testarD() {
		System.out.println("D");		
	}
}
