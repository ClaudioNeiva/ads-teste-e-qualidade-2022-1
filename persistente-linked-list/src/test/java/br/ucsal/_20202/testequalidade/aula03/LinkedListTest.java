package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

class LinkedListTest {

	private List<String> nomes;

	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}

	@Test
	void testarInclusao1Nome() throws InvalidElementException {
		// Dados de entrada
		String nomeInformado = "claudio";

		// Saída esperada
		String nomeEsperado = "claudio";

		// Executar o método que está sendo testado
		nomes.add(nomeInformado);

		// Recuperar o saída atual
		String nomeAtual = nomes.get(0);

		// Comprar a saída esperada com a saída atual
		Assertions.assertEquals(nomeEsperado, nomeAtual);
	}

	@Test
	@Disabled
	void testarInclusao3Nomes() throws InvalidElementException {
		// Dados de entrada
		String nomeInformado1 = "claudio";
		String nomeInformado2 = "antonio";
		String nomeInformado3 = "neiva";

		// Saída esperada
		String nomeEsperado1 = "claudio";
		String nomeEsperado2 = "antonio";
		String nomeEsperado3 = "neiva";

		// Executar o método que está sendo testado
		nomes.add(nomeInformado1);
		nomes.add(nomeInformado2);
		nomes.add(nomeInformado3);

		// Recuperar o saída atual
		String nomeAtual1 = nomes.get(0);
		String nomeAtual2 = nomes.get(1);
		String nomeAtual3 = nomes.get(2);

		// Comprar a saída esperada com a saída atual
		// assertEquals(nomeEsperado1, nomeAtual1, "Nome esperado é diferente do nome
		// atual");
		assertAll(() -> Assertions.assertEquals(nomeEsperado1, nomeAtual1),
				() -> Assertions.assertEquals(nomeEsperado2, nomeAtual2),
				() -> Assertions.assertEquals(nomeEsperado3, nomeAtual3));
	}

	@Test
	@DisplayName("Testar exceção para inclusão de nome nulo na lista.")
	void testarExcecao1() {
		// Dados de entrada
		String nomeEntrada = null;

		// Saída esperada => a ocorrênia da exceção
		String mensagemEsperada = "The element can't be null.";

		// Executar o método a ser testado, coletar a saída atual e comparar com a saída
		// esperada
		InvalidElementException exception = Assertions.assertThrows(InvalidElementException.class,
				() -> nomes.add(nomeEntrada));

		Assertions.assertEquals(mensagemEsperada, exception.getMessage());
	}

}
