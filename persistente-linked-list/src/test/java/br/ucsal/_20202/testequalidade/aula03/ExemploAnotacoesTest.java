package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ExemploAnotacoesTest {

	@BeforeAll
	static void setupClasse() {
		System.out.println("setupClasse()");
	}

	@BeforeEach
	void setup() {
		System.out.println("	setup()");
	}

	@AfterEach
	void tearDown() {
		System.out.println("	tearDown()");
	}

	@AfterAll
	static void tearDownClasse() {
		System.out.println("tearDownClasse()");
	}

	@Test
	void teste1() {
		System.out.println("		teste1()");
	}

	@Test
	void teste2() {
		System.out.println("		teste2()");
	}

	@Test
	void teste3() {
		System.out.println("		teste3()");
	}

}
