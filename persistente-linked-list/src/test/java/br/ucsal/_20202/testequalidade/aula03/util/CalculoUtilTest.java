package br.ucsal._20202.testequalidade.aula03.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CalculoUtilTest {

	@ParameterizedTest(name = "testar fatorial de {0}")
	@CsvSource({ "0,1", "3,6", "5,120" })
	void testarFatorial(int n, long fatorialEsperado) {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}
	
}
