package br.ucsal.bes20192.testequalidade.escola.mock;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOMock extends AlunoDAO {

	private Aluno alunoTest;

	private Map<Aluno, Integer> chamadasSalvar = new HashMap<>();

	public void setup(Aluno alunoTest) {
		this.alunoTest = alunoTest;
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		return alunoTest;
	}

	@Override
	public void salvar(Aluno aluno) {
		chamadasSalvar.putIfAbsent(aluno, 0);
		chamadasSalvar.put(aluno, chamadasSalvar.get(aluno) + 1);
	}

	public void verificarChamadasSalvar(int qtdEsperado, Aluno alunoEsperado) {
		if (!chamadasSalvar.containsKey(alunoEsperado)) {
			Assertions.fail("Não ocorreram chamadas ao " + alunoEsperado);
		}
		Assertions.assertEquals(qtdEsperado, chamadasSalvar.get(alunoEsperado),
				"Quantidade esperada de chamadas ao salvar diferente da atual.");
	}

}
