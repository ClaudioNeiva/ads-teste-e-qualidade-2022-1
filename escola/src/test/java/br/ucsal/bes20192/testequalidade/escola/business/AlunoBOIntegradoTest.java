package br.ucsal.bes20192.testequalidade.escola.business;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AlunoBOIntegradoTest {

	private AlunoDAO alunoDAO;
	private AlunoBO alunoBO;

	@BeforeAll
	void setupAll() {
		alunoDAO = new AlunoDAO();
		alunoBO = new AlunoBO(alunoDAO);
	}

	@BeforeEach
	void setup() {
		alunoDAO.excluirTodos();
	}

	@AfterAll
	void tearDownAll() {
		alunoDAO.excluirTodos();
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em h� 16 anos.
	 */
	// 157
	// 239
	// 162
	@Test
	void testarCalculoIdadeAluno1() {
		Integer matricula = 123;
		Integer anoNascimento = LocalDate.now().getYear() - 16;
		Integer idadeEsperada = 16;

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(anoNascimento).build();
		alunoDAO.salvar(aluno1);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 456;
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		alunoBO.atualizar(alunoEsperado);

		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(matricula);

		Assertions.assertEquals(alunoEsperado, alunoAtual);
	}

}
