package br.ucsal.bes20192.testequalidade.escola.tui;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class TuiHelper2Test {

	private PrintStream outOriginal;

	@Mock
	private Scanner scannerMock;

	@Mock
	private PrintStream outMock;

	@InjectMocks
	private TuiHelper tuiHelper;

	@BeforeEach
	void setup() {
		// MockitoAnnotations.initMocks(this);
		tuiHelper = new TuiHelper();
		outOriginal = System.out;
		System.setOut(outMock);
	}

	@AfterAll
	void teardown() {
		System.setOut(outOriginal);
	}

	@Test
	@Disabled
	void testarObtencaoNomeCompleto() {

		String nome = "Claudio";
		String sobrenome = "Neiva";

		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(sobrenome);

		String nomeCompletoEsperado = "Claudio Neiva";
		String nomeCompletoAtual = tuiHelper.obterNomeCompleto();

		Assertions.assertAll(() -> Assertions.assertEquals(nomeCompletoEsperado, nomeCompletoAtual),
				() -> Mockito.verify(outMock).println("Informe o nome:"),
				() -> Mockito.verify(outMock).println("Informe o sobrenome:"));
	}

}
