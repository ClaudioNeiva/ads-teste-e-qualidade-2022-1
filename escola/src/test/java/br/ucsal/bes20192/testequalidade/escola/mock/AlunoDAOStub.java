package br.ucsal.bes20192.testequalidade.escola.mock;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOStub extends AlunoDAO {

	private Aluno alunoTest;

	public void setup(Aluno alunoTest) {
		this.alunoTest = alunoTest;
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		return alunoTest;
	}

	@Override
	public void salvar(Aluno aluno) {
	}

}
