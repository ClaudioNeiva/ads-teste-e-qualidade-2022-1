package br.ucsal.bes20192.testequalidade.escola.business;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AlunoBOTest {

	private AlunoDAO alunoDAOMock;
	private AlunoBO alunoBO;

	@BeforeAll
	void setupAll() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		alunoBO = new AlunoBO(alunoDAOMock);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em h� 16 anos.
	 */
	// 157 - 0
	// 239 - 21
	// 162 - 24
	@Test
	void testarCalculoIdadeAluno1() {
		Integer matricula = 123;
		Integer anoNascimento = LocalDate.now().getYear() - 16;
		Integer idadeEsperada = 16;

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(anoNascimento).build();
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno1);

		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 456;
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		alunoBO.atualizar(alunoEsperado);
		
		// Verificar através de índicios. Por exemplo, saber se ocorreu uma chamada ao
		// salvar.
		Mockito.verify(alunoDAOMock).salvar(alunoEsperado);
	}
}
