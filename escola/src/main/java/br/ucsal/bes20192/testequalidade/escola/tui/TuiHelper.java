package br.ucsal.bes20192.testequalidade.escola.tui;

import java.util.Scanner;

public class TuiHelper {

	public Scanner scanner = new Scanner(System.in);

	public String obterNomeCompleto() {
		System.out.println("Informe o nome:");			//CM-1 -> motivação
		String nome = scanner.nextLine();				//CM-2
		System.out.println("Informe o sobrenome:");		//CM-3
		String sobrenome = scanner.nextLine();			//CM-4
		return nome + " " + sobrenome;
	}

	public void exibirMensagem(String mensagem) {
		System.out.print("Bom dia! ");
		System.out.println(mensagem);
	}

}
