package br.ucsal.bes20192.testequalidade.escola.business;

import java.time.LocalDate;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoBO {

	private AlunoDAO alunoDAO;

	public AlunoBO(AlunoDAO alunoDAO) {
		this.alunoDAO = alunoDAO;
	}

	// Método command (método SEM retorno). Como o teste saberá que teve sucesso?
	public void atualizar(Aluno aluno) {
		if (SituacaoAluno.ATIVO.equals(aluno.getSituacao())) {
			alunoDAO.salvar(aluno);
		}
	}

	// Método query (método com retorno). Para o teste basta comprar esse retorno
	// com o valor esperado.
	public Integer calcularIdade(Integer matricula) {
		Aluno aluno = alunoDAO.encontrarPorMatricula(matricula);
		return LocalDate.now().getYear() - aluno.getAnoNascimento();
	}

}
